package id.trimegah;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

import javax.persistence.Entity;
import javax.validation.constraints.*;

@Entity
public class Book extends PanacheEntity {


    @NotBlank(message="Title cannot be blank")
    public String title;

    @NotBlank(message="Author cannot be blank")
    public String author;

    @Min(message="Author has been very lazy", value=1)
    public double pages;
}