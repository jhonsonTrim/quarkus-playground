package id.trimegah;


import io.quarkus.panache.common.Sort;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.ConstraintViolation;
import javax.validation.Valid;
import javax.validation.Validator;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static id.trimegah.Person.*;


@Path("/person")
@ApplicationScoped
@Produces("application/json")
@Consumes("application/json")
public class PersonResource {
    @Inject
    Validator validator;

    @GET
    public List<Person> getAll() {
        return listAll(Sort.by("id"));
    }

    @POST
    @Path("/add")
    @Produces("application/json")
    @Transactional
    public Response addPerson(Person person){
        persist(person);
         return Response.status(Response.Status.CREATED).entity(person).build();
    }

    @Path("/validate")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public PersonResource.Result tryMeManualValidation(Person person) {

        Set<ConstraintViolation<Person>> violations = validator.validate(person);
        if (violations.isEmpty()) {
            return new PersonResource.Result("Book is valid! It was validated by manual validation.");
        } else {
            return new PersonResource.Result(violations);
        }
    }

    @Path("/validate-response")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response tryValidationResponse(Person person) {

        Set<ConstraintViolation<Person>> violations = validator.validate(person);
        if (violations.isEmpty()) {
            return Response.accepted().build();
        } else {
            return Response.serverError().build();
        }
    }


    public static class Result {

        private String message;
        private boolean success;

        Result(String message) {
            this.success = true;
            this.message = message;
        }

        Result(Set<? extends ConstraintViolation<?>> violations) {
            this.success = false;
            this.message = violations.stream()
                    .map(ConstraintViolation::getMessage)
                    .collect(Collectors.joining(", "));
        }

        public String getMessage() {
            return message;
        }

        public boolean isSuccess() {
            return success;
        }

    }
}
