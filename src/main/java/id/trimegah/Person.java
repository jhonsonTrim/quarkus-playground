package id.trimegah;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.validation.constraints.*;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

@Entity
@Cacheable
public class Person  extends PanacheEntity{

    @NotBlank(message = "Name cannot be empty, or this field is required")
    private String name;
    @NotBlank(message = "Birth Place cannot be empty, or this field is required")
    private String birthPlace;
    private String sts;

    @Min(message="Age Required, min 17", value=17)
    @NotBlank
    @NotEmpty
    @NotNull(message="Age is required")
    private Integer age;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public String getSts() {
        return sts;
    }

    public void setSts(String sts) {
        this.sts = sts;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
